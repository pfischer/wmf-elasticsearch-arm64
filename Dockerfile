FROM docker.elastic.co/elasticsearch/elasticsearch:7.10.2

ENV NODE_NAME=elastic
RUN bin/elasticsearch-plugin install \
    https://repo1.maven.org/maven2/org/wikimedia/search/extra/7.10.2-wmf1/extra-7.10.2-wmf1.zip && \
    bin/elasticsearch-plugin install \
    https://repo1.maven.org/maven2/org/wikimedia/search/highlighter/experimental-highlighter-elasticsearch-plugin/7.10.2/experimental-highlighter-elasticsearch-plugin-7.10.2.zip
